import os

from selenium import webdriver
import time


links_list = []
# def load_links(arr):
#     for src in arr:
#         print( src.get_attribute("href"))
#         print(src)
#         print()


if __name__ == '__main__':

    driver = webdriver.Chrome()

    url = driver.command_executor._url
    session_id = driver.session_id

    # driver.get("https://www.facebook.com/hackercup/scoreboard/1760504744276109/?filter=everyone")
    # driver.get("https://www.facebook.com/hackercup/scoreboard/1825579961046099/?filter=everyone")
    # driver.get("https://www.facebook.com/hackercup/scoreboard/742957399195355/?filter=everyone")
    # driver.get("https://www.facebook.com/hackercup/scoreboard/904578626288920/?filter=everyone")
    # driver.get("https://www.facebook.com/hackercup/scoreboard/165233060474397/?filter=everyone")
    # driver.get("https://www.facebook.com/hackercup/scoreboard/807323692699472/?filter=everyone")
    # driver.get("https://www.facebook.com/hackercup/scoreboard/742632349177460/?filter=everyone")
    # driver.get("https://www.facebook.com/hackercup/scoreboard/323882677799153/?filter=everyone")
    # driver.get("https://www.facebook.com/hackercup/scoreboard/598486203541358/?filter=everyone")
    # driver.get("https://www.facebook.com/hackercup/scoreboard/1437956993099239/?filter=everyone")
    # driver.get("https://www.facebook.com/hackercup/scoreboard/544142832342014/?filter=everyone")
    # driver.get("https://www.facebook.com/hackercup/scoreboard/185564241586420/?filter=everyone")
    # driver.get("https://www.facebook.com/hackercup/scoreboard/189890111155691/?filter=everyone")
    # driver.get("https://www.facebook.com/hackercup/scoreboard/499927843385312/?filter=everyone")
    # driver.get("https://www.facebook.com/hackercup/scoreboard/146094915502528/?filter=everyone")  # 2012 qual
    # driver.get("https://www.facebook.com/hackercup/scoreboard/225705397509134/?filter=everyone")  # 2012 1
    # driver.get("https://www.facebook.com/hackercup/scoreboard/154897681286317/?filter=everyone")  # 2012 2
    driver.get("https://www.facebook.com/hackercup/scoreboard/4/?filter=everyone")  # 2011 qual
    # time.sleep(30)

    i = 0

    input("Login and press enter:")

    while i*50 + 50 < 11786:
        LongDsc = driver.find_elements_by_link_text("source")

        print('LongDsc:', LongDsc)
        print(len(LongDsc))

        # load_links(LongDsc)
        for src in LongDsc:
            try:
                links_list.append(src.get_attribute("href"))
            except:
                print("Exception on ", src)
                print("i = ", i)

        next_btn = driver.find_elements_by_css_selector("button[class='_4jy0 _4jy3 _517h _51sy _42ft']")

        next_btn = list(filter(lambda btn: btn.text == 'Next page', next_btn))[0]
        driver.execute_script("arguments[0].click();", next_btn)

        time.sleep(3)

        i += 1

    with open("links_list_2011_qual.txt", "w+") as f:
        for link in links_list:
            print(link, file=f)
