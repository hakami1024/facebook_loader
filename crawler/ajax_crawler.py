# Importing the dependencies
# This is needed to create a lxml object that uses the css selector
import json
from time import sleep

from lxml.etree import fromstring

# The requests library
import requests


class Scraper:
    API_url = 'https://www.facebook.com/hackercup/x/scoreboard/page/?dpr=1'

    def get_page(self, count):
        # This is the only data required by the api
        # To send back the stores info
        # Making the post request
        response = requests.post(
            'https://www.facebook.com/hackercup/x/scoreboard/page/?dpr=1',
            # json=
            headers={
                    # ":authority": "www.facebook.com",
                    #  ":method": "POST",
                    #  ":path": "/hackercup/x/scoreboard/page/?dpr=1",
                    #  ":scheme": "https",
                     "accept": "*/*",
                     "accept-encoding": "gzip, deflate, br",
                     "accept-language": "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
                     "content-length": "505",
                     "content-type": "application/x-www-form-urlencoded",
                     "cookie": "datr=ZtAxW1P9ClEfHPo7NgRkXGZI; wd=1234x994; sb=XtQxWzuRvK7Ttw1LcgDnQb_R; c_user=100007083731814; xs=6%3AmINIP3idyhCxeA%3A2%3A1529992287%3A16405%3A14492; fr=04cAbAD38thLnylGM.AWUseoTba7EZjQVXImbpaT5wzIA.BbI6km.G9.AAA.0.0.BbMdRe.AWUIgMlB; pl=n; spin=r.4046581_b.trunk_t.1529992288_s.1_v.2_; presence=EDvF3EtimeF153B4304EuserFA21B07083731814A2EstateFDutF153B4304008CEchFDp_5f1B07083731814F8CC",
                     "origin": "https://www.facebook.com",
                     "referer": "https://www.facebook.com/hackercup/scoreboard/1760504744276109/?filter=everyone",
                     "user-agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.79 Safari/537.36}"},
            data={
                "round_id": "1760504744276109",
                "mode": "finalized",
                "filter": "everyone",
                "offset": str(count),
                "length": "50",
                "from_assocs": "1",
                "__user": "100007083731814",
                "__a": "1",
                "__dyn": "7AgNe - 4am2d2u6aJGeFxqewRyWzEpF4Wo8opBxGdwIhE98nwgU6C7WUC3q2OUuxa3KbwTz8S2S4o5eu58O5U7e9xa2m4o6e2e1tG0HFU2_CK6ooxu6U6O11x - 8wywrESexeEgwsEgy8ix69wyUa9u4S3CcByo8U8Kq0YEuzHAyE5a",
                "__req": "1g",
                "__be": "1",
                "__pc": "PHASED:DEFAULT",
                "__rev": "4046581",
                "fb_dtsg": "AQGT_nJcPqvZ:AQFQONRRBYtx",
                "jazoest": "26581718495110749980113118905865817081797882826689116120",
                "__spin_r": "4046581",
                "__spin_b": "trunk",
                "__spin_t": "1529992288",
            }
        )

        # The data that we are looking is in the second
        # Element of the response and has the key 'data',
        # so that is what's returned
        return response

    def save_code(self, data):
        # Creating an lxml Element instance

        element = fromstring(data)

        for store in element.cssselect('.views-row'):
            store_info = {}

            # The lxml etree css selector always returns a list, so we get
            # just the first item
            store_name = store.cssselect('.views-field-title a')[0].text
            street_address = store.cssselect('.street-block div')[0].text
            address_locality = store.cssselect('.locality')[0].text
            address_state = store.cssselect('.state')[0].text
            address_postal_code = store.cssselect('.postal-code')[0].text
            phone_number = store.cssselect('.views-field-field-phone-number div')[0].text

            try:
                opening_hours = store.cssselect('.views-field-field-store-hours div')[0].text
            except IndexError:
                # Stores that doesn't have opening hours are closed and should not be saved.
                # This is found while debugging, so don't worry if you get errors when you
                # run a scraper
                opening_hours = 'STORE CLOSED'
                continue

            full_address = "{}, {}, {} {}".format(street_address,
                                                  address_locality,
                                                  address_state,
                                                  address_postal_code)

            # now we add all the info to a dict
            store_info = {
                'name': store_name,
                'full_address': full_address,
                'phone': phone_number,
                'hours': opening_hours
            }

    def run(self):
        count = 50

        while count < 12804:
            # Retrieving the data
            data = self.get_page(count)
            # Parsing it
            print(data.status_code)
            self.save_code(data)
            print('scraped the page' + str(count))

            sleep(1)

            count +=50


if __name__ == '__main__':
    scraper = Scraper()
    scraper.run()

