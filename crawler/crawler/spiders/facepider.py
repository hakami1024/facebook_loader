# -*- coding: utf-8 -*-
import re

from scrapy import FormRequest
from scrapy.spiders import CrawlSpider, Rule, BaseSpider, Spider


class FacepiderSpider(Spider):
    name = 'facepider'
    start_urls = ['https://www.facebook.com/hackercup/scoreboard/1760504744276109/?filter=everyone']
    count = 0

    def parse(self, response):
        if self.count >= 12804:
            return None

        yield FormRequest('''https://www.facebook.com/hackercup/x/scoreboard/page/?dpr=1''',
                        headers={":authority": "www.facebook.com",
                                 ":method": "POST",
                                 ":path": "/hackercup/x/scoreboard/page/?dpr=1",
                                 ":scheme": "https",
                                 "accept": "*/*",
                                 "accept-encoding": "gzip, deflate, br",
                                "accept-language":" ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
                                "content-length":" 505",
                                "content-type":" application/x-www-form-urlencoded",
                                "cookie": "datr=ZtAxW1P9ClEfHPo7NgRkXGZI; wd=1234x994; sb=XtQxWzuRvK7Ttw1LcgDnQb_R; c_user=100007083731814; xs=6%3AmINIP3idyhCxeA%3A2%3A1529992287%3A16405%3A14492; fr=04cAbAD38thLnylGM.AWUseoTba7EZjQVXImbpaT5wzIA.BbI6km.G9.AAA.0.0.BbMdRe.AWUIgMlB; pl=n; spin=r.4046581_b.trunk_t.1529992288_s.1_v.2_; presence=EDvF3EtimeF153B4304EuserFA21B07083731814A2EstateFDutF153B4304008CEchFDp_5f1B07083731814F8CC",
                                "origin":" https://www.facebook.com",
                                "referer":" https://www.facebook.com/hackercup/scoreboard/1760504744276109/?filter=everyone",
                                "user-agent":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.79 Safari/537.36}"},
                        callback=self.RubiGuessItem,
                        formdata={
                            "round_id":" 1760504744276109",
                            "mode":" finalized",
                            "filter":" everyone",
                            "offset": str(self.count),
                            "length":" 50",
                            "from_assocs":" 1",
                            "__user":" 100007083731814",
                            "__a":" 1",
                            "__dyn":" 7AgNe - 4am2d2u6aJGeFxqewRyWzEpF4Wo8opBxGdwIhE98nwgU6C7WUC3q2OUuxa3KbwTz8S2S4o5eu58O5U7e9xa2m4o6e2e1tG0HFU2_CK6ooxu6U6O11x - 8wywrESexeEgwsEgy8ix69wyUa9u4S3CcByo8U8Kq0YEuzHAyE5a",
                            "__req":" 1g",
                          "__be":" 1",
                          "__pc":" PHASED:DEFAULT",
                          "__rev":" 4046581",
                          "fb_dtsg":" AQGT_nJcPqvZ:AQFQONRRBYtx",
                          "jazoest":" 26581718495110749980113118905865817081797882826689116120",
                          "__spin_r":" 4046581",
                          "__spin_b":" trunk",
                          "__spin_t":" 1529992288",
                        })
        self.count += 50

    def RubiGuessItem(self, response):
       json_file = response.body
       print('Processing..' + response.url)
