import os
import threading
from queue import Queue
from threading import Thread
from time import sleep

import requests

semaphore = threading.Semaphore(4)


def load_files():
    while True:
        semaphore.acquire(blocking=True)
        global q

        link = q.get(block = True)

        r = requests.get(link, params={'user-agent':
                                           'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.79 Safari/537.36',
                                       'authority': 'www.facebook.com',
                                       'cookie': 'datr=ZtAxW1P9ClEfHPo7NgRkXGZI; sb=XtQxWzuRvK7Ttw1LcgDnQb_R; c_user=100007083731814; xs=35%3AKYEyEg8pkY5NQQ%3A2%3A1530189727%3A16405%3A14492; pl=n; spin=r.4093094_b.trunk_t.1531350469_s.1_v.2_; fr=04cAbAD38thLnylGM.AWUb-CxCq-sOwoXXkLUX1HH22LE.BbI6km.G9.Fs2.0.0.BbRxZm.AWV7dsjw; presence=EDvF3EtimeF1531396235EuserFA21B07083731814A2EstateFDutF1531396235130CEchFDp_5f1B07083731814F2CC; wd=761x1014; act=1531396240950%2F3; x-src=%2Fhackercup%2Fscoreboard%2F838198113036494%2F%7Ccontent; pnl_data2=eyJhIjoib25hZnRlcmxvYWQiLCJjIjoiWEhhY2tlckN1cFByb2JsZW1Db250cm9sbGVyIiwiYiI6ZmFsc2UsImQiOiIvaGFja2VyY3VwL3Njb3JlYm9hcmQvODM4MTk4MTEzMDM2NDk0LyIsImUiOltdfQ%3D%3D'})

        if r.status_code != 200:
            print(link, r.status_code)
            semaphore.release()
            continue

        with open("./fb_sources/"+link, 'w') as f:
            print(r.content.decode(r.encoding), file=f)

        semaphore.release()
        sleep(2)


def fill_links(fname):
    with open('./'+fname, "r") as f:
        links_list = list(f.readlines())
        print(fname, len(links_list))
        for lnk in links_list:
            q.put(lnk)


if __name__ == '__main__':
    q = Queue()

    files = list(filter(lambda x: 'links_list' in x, os.listdir('.')))

    f_count = len(files)

    first_links = None

    for f in files:
        t = Thread(target=fill_links, args=(f,), daemon=True)
        t.start()

        if not first_links:
            first_links = t

    [Thread(target=load_files, daemon=True).start() for i in range(4)]

    first_links.join()

    while q.not_empty:
        sleep(5)